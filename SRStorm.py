from audioop import avg
from cgitb import text
from curses import curs_set
import curses
# from logging import _Level
from re import A, L
from typing_extensions import Self
from unittest import result
from urllib.request import urlopen as req
from bs4 import BeautifulSoup as soup
from django.urls import clear_script_prefix
import array as arr
from numpy import number
import psycopg2
from psycopg2 import Error
import datetime
from time import time
import mysql.connector
from mysql.connector import Error

def connect():
    """ Connect to MySQL database """
    conn = None
    try:
        conn = mysql.connector.connect(host='remotemysql.com',
                                       port='3306',
                                       database='fvpnSn9xtp',
                                       user='fvpnSn9xtp',
                                       password='x6DdkN6mXz')
        if conn.is_connected():
            print('Connected to MySQL database')
        mycursor = conn.cursor()
        url = 'https://services.swpc.noaa.gov/text/3-day-forecast.txt'

        #2 - REQUEST URL
        webopen = req(url)
        page_html = webopen.read()
        webopen.close()

        #3 - CONVERT page.html to Soup Object  
        #print(page_html)
        data = soup(page_html , 'html.parser')
        array = []
        
        def time():
            today = datetime.datetime.today()
            hour1 = today.hour
            minute = today.minute
            for (i, item) in enumerate(data, start=1):
                any = item.text.split()
                # print(any)
                a2 = any[174] 
                a3 = any[171]
                
                print(a2,a3)
            if hour1 <= 24:
                if minute == 00:         
                    if a3 == 'S1':
                        level_srs = "เกิดขึ้นประมาณ 50 เหตุการณ์"
                        sql = "insert into SRStorm(srs,avg_srs,level_storm) value(%s,%s,%s)" 
                        val=(a2,a3,level_srs)
                        mycursor.execute(sql,val)
                        conn.commit(); 
                    elif a3 == 'S2':
                        level_srs = "เกิดขึ้นประมาณ 25 เหตุการณ์"
                        sql = "insert into radio(srs,avg_srs,level_storm) value(%s,%s,%s)" 
                        val=(a2,a3,level_srs)
                        mycursor.execute(sql,val)
                        conn.commit(); 
                    elif a3 == 'S3':
                        level_srs = "เกิดขึ้นประมาณ 10 เหตุการณ์"
                        sql = "insert into radio(srs,avg_srs,level_storm) value(%s,%s,%s)" 
                        val=(a2,a3,level_srs)
                        mycursor.execute(sql,val)
                        conn.commit(); 
                    elif a3 == 'S4':
                        level_srs = "เกิดขึ้นประมาณ 3 เหตุการณ์"
                        sql = "insert into radio(srs,avg_srs,level_storm) value(%s,%s,%s)" 
                        val=(a2,a3,level_srs)
                        mycursor.execute(sql,val)
                        conn.commit(); 
                    elif a3 == 'S5':
                        level_srs = "เกิดขึ้นประมาณ 1 เหตุการณ์"
                        sql = "insert into radio(srs,avg_srs,level_storm) value(%s,%s,%s)" 
                        val=(a2,a3,level_srs)
                        mycursor.execute(sql,val)
                        conn.commit(); 
        
        time()
    except Error as e:
        print(e)

    finally:
        if conn is not None and conn.is_connected():
            conn.close()


if __name__ == '__main__':
    connect()