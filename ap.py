import mysql.connector
from mysql.connector import Error
from calendar import month
from cgitb import text
from curses import curs_set
import curses
from re import L
from typing_extensions import Self
from unittest import result
from urllib.request import urlopen as req
from bs4 import BeautifulSoup as soup
from django.urls import clear_script_prefix
import array as arr
import psycopg2
from psycopg2 import Error
import datetime

def connect():
    """ Connect to MySQL database """
    conn = None
    try:
        conn = mysql.connector.connect(host='remotemysql.com',
                                       port='3306',
                                       database='fvpnSn9xtp',
                                       user='fvpnSn9xtp',
                                       password='x6DdkN6mXz')
        if conn.is_connected():
            print('Connected to MySQL database')
        mycursor = conn.cursor()
        url = 'https://services.swpc.noaa.gov/text/3-day-geomag-forecast.txt'

                #2 - REQUEST URL
        webopen = req(url)
        page_html = webopen.read()
        webopen.close()

        #    #3 - CONVERT page.html to Soup Object
        # print(page_html)
        data = soup(page_html , 'html.parser')
        # print(data)
        r = data.text
        p1 = r.replace('NOAA Ap Index Forecast', ' ')
        p2 = p1.replace('Observed Ap 20 Mar 009', ' ')
        p3 = p2.replace('Estimated Ap 21 Mar 004', ' ')
        p4 = p3.replace('NOAA Geomagnetic Activity Probabilities 22 Mar-24 Mar', ' ')
        p5 = p4.replace('-', ' ')
        p6 = r.replace('NOAA Ap Index Forecast', ' ')
        p7 = r.replace('NOAA Ap Index Forecast', ' ')
        p8 = r.replace('NOAA Ap Index Forecast', ' ')
        p9 = r.replace('NOAA Ap Index Forecast', ' ')
                # Find all data for each column
        # print(p5)    
        input_string = p5
        print("\n")
        
        user_list = input_string.split()
        # print (user_list)
        M1 = user_list[25]
        M2 = user_list[26]
        M3 = user_list[27]
        M4 = user_list[28]
        month = M1 + '' + M2 + '-' + M3 + '' + M4 
        ap1 = user_list[39]
        ap2 = user_list[40]
        ap3 = user_list[41]
        # rb2 = user_list[1]
        # rb3 = user_list[2]
        print(ap1)
        level = ap1
        
        def time():
            today = datetime.datetime.today()
            hour1 = today.hour
            min = today.minute
        
            if hour1 <= 24 and min == 00:
                if level <= '56':
                    grade = "G1"
                    lvel_ap = "เงียบ ถึง เล็กน้อย"
                    print(lvel_ap ,grade)
                    sql = "insert into ap(ap,avg_ap,level_ap) value(%s,%s,%s)" 
                    val=(level ,grade ,lvel_ap)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif level <= '94':
                    grade = "G2"
                    lvel_ap = "ปานกลาง"
                    print(lvel_ap,grade)
                    sql = "insert into ap(ap,avg_ap,level_ap) value(%s,%s,%s)" 
                    val=(level ,grade ,lvel_ap)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                    
                elif level <= '154':
                    grade = "G3"
                    lvel_ap = "รุนแรง"
                    print(lvel_ap,grade)
                    sql = "insert into ap(ap,avg_ap,level_ap) value(%s,%s,%s)" 
                    val=(level ,grade ,lvel_ap)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif level <= '300' :
                    grade = "G4"
                    lvel_ap = "รุนแรงมาก"
                    print(lvel_ap,grade)
                    sql = "insert into ap(ap,avg_ap,level_ap) value(%s,%s,%s)" 
                    val=(level ,grade ,lvel_ap)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else:
                    grade = "G5"
                    lvel_ap = "รุนแรงที่สุด"
                    print(lvel_ap,grade)
                    sql = "insert into ap(ap,avg_ap,level_ap) value(%s,%s,%s)" 
                    val=(level ,grade ,lvel_ap)
                    mycursor.execute(sql,val)
                    conn.commit(); 
        time()
    except Error as e:
        print(e)

    finally:
        if conn is not None and conn.is_connected():
            conn.close()


if __name__ == '__main__':
    connect()