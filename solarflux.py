import mysql.connector
from mysql.connector import Error
from calendar import month
from datetime import date, datetime
from posixpath import split
from time import time
from pytz import HOUR
import requests
from bs4 import BeautifulSoup
import pandas as pd
import psycopg2
from psycopg2 import Error
from urllib.request import urlopen as req
# json
import json
import numpy as np
from cgitb import text
import datetime

def connect():
    """ Connect to MySQL database """
    conn = None
    try:
        conn = mysql.connector.connect(host='remotemysql.com',
                                       port='3306',
                                       database='fvpnSn9xtp',
                                       user='fvpnSn9xtp',
                                       password='x6DdkN6mXz')
        if conn.is_connected():
            print('Connected to MySQL database')
        mycursor = conn.cursor()
        url = "https://www.spaceweather.gc.ca/forecast-prevision/solar-solaire/solarflux/sx-5-flux-en.php"
        data = requests.get(url).text
        webopen = req(url)
        page_html = webopen.read()
        webopen.close()

        # Creating BeautifulSoup object
        soup = BeautifulSoup(data, 'html.parser')

        print('Classes of each table:')
        for table in soup.find_all('table'):
            print(table.get('class'))

        # Creating list with all tables
        tables = soup.find_all('table')
        def sum():
        #  Looking for the table with the classes 'wikitable' and 'sortable'
            table = soup.find('table', class_='table-bordered')
            for row in table.find_all('tr')[1:]:
                month = row.find_all('th')
                # Find all data for each column
                columns = row.find_all('td')[4]
                a = columns
            for item in a:
                        adjust = item.text.split()
                        data = adjust[0]
            return data              
          
            
        list1 = sum()
        # list = str(li)
        # # print(list)
        def time():
            today = datetime.datetime.today()
            hour1 = today.hour
            minute = today.minute
            list = float(list1)
            # print(hour1)
            if 18 <= hour1 <= 20:
                if minute == 00:
                    if list <= 50.9 :
                        grade = "G1"
                        level = "เงียบ ถึง เล็กน้อย"
                        sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                        val=(list, grade, level)
                        mycursor.execute(sql,val)
                        conn.commit(); 
                    elif 50.9 < list <= 100.9:
                        grade = "G2"
                        level = "ปานกลาง"
                        sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                        val=(list, grade, level)
                        mycursor.execute(sql,val)
                        conn.commit(); 
                    elif 100.9 < list <= 150.9:
                        grade = "G3"
                        level = "รุนแรง"
                        sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                        val=(list, grade, level)
                        mycursor.execute(sql,val)
                        conn.commit();  
                    elif 150.9 < list <= 200.9 :
                        grade = "G4"
                        level = "รุนแรงมาก"
                        sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                        val=(list, grade, level)
                        mycursor.execute(sql,val)
                        conn.commit(); 
                    elif list > 250.9:
                        grade = "G5"
                        level = "รุนแรงที่สุด"
                        sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                        val=(list, grade, level)
                        mycursor.execute(sql,val)
                        conn.commit();  
                elif list <= 50.9 :
                    grade = "G1"
                    level = "เงียบ ถึง เล็กน้อย"
                    sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                    val=(list, grade, level)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif 50.9 < list <= 100.9:
                    grade = "G2"
                    level = "ปานกลาง"
                    sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                    val=(list, grade, level)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif 100.9 < list <= 150.9:
                    grade = "G3"
                    level = "รุนแรง"
                    sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                    val=(list, grade, level)
                    mycursor.execute(sql,val)
                    conn.commit();  
                elif 150.9 < list <= 200.9 :
                    grade = "G4"
                    level = "รุนแรงมาก"
                    sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                    val=(list, grade, level)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif list > 250.9:
                    grade = "G5"
                    level = "รุนแรงที่สุด"
                    sql = "insert into Solarflux(adjusted_flux,avg_flux,level_flux) value(%s,%s,%s)" 
                    val=(list, grade, level)
                    mycursor.execute(sql,val)
                    conn.commit();  
            
        time()
        
    except Error as e:
        print(e)

    finally:
        if conn is not None and conn.is_connected():
            conn.close()


if __name__ == '__main__':
    connect()