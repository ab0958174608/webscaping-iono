from re import A
from sys import api_version
import numpy as np
import datetime
from time import time
import pandas as pd
import joblib
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
import mysql.connector
from mysql.connector import Error
def connect():
    """ Connect to MySQL database """
    conn = None
    try:
        conn = mysql.connector.connect(host='remotemysql.com',
                                       port='3306',
                                       database='fvpnSn9xtp',
                                       user='fvpnSn9xtp',
                                       password='x6DdkN6mXz',
                                       buffered=True)
        if conn.is_connected():
            print('Connected to MySQL database')
            

        def time():
            today = datetime.datetime.today()
            hour = today.hour 
            min = today.minute  
            print(hour,min)
            if hour == 20 and min == 00 : 
                cursor = conn.cursor()
                cursor.execute("SELECT dst , ap ,kp , adjusted_flux, r1_r2 FROM dst join ap join kp_index join solarflux join radio ORDER BY id_dst DESC, id_ap DESC, id_kp DESC, id_flux DESC,radio_id")
                myresult =  cursor.fetchone()
                sum1 = myresult[0]
                dst = float(sum1)
                ap = myresult[1]
                sum2 = myresult[2]
                kp = float(sum2)
                sum3 = myresult[3]
                f107 = float(sum3)
                sum4 = myresult[4]
                A = float(sum4.replace('%', ' '))
                radio = A / 100
                print(radio) 

                model = joblib.load('rfs.joblib')
                prediction = model.predict([[dst, ap, f107, radio, kp]])
                KNN = (prediction[0])
                sql = """insert into Knn(knn) value (%s)"""
                cursor.execute(sql,[KNN])
                conn.commit(); 
                print(KNN)
        time()
    except Error as e:
        print(e)

    finally:
        if conn is not None and conn.is_connected():
            conn.close()


if __name__ == '__main__':
    connect()