from audioop import avg
from cgitb import text
from curses import curs_set
import curses
from re import A, L
from typing_extensions import Self
from unittest import result
from urllib.request import urlopen as req
from bs4 import BeautifulSoup as soup
from django.urls import clear_script_prefix
import array as arr
from numpy import number
import psycopg2
from psycopg2 import Error
import datetime
from time import time
import mysql.connector
from mysql.connector import Error

def connect():
    """ Connect to MySQL database """
    conn = None
    try:
        conn = mysql.connector.connect(host='remotemysql.com',
                                       port='3306',
                                       database='fvpnSn9xtp',
                                       user='fvpnSn9xtp',
                                       password='x6DdkN6mXz')
        if conn.is_connected():
            print('Connected to MySQL database')
        mycursor = conn.cursor()
        url = 'https://services.swpc.noaa.gov/text/3-day-forecast.txt'

        #2 - REQUEST URL
        webopen = req(url)
        page_html = webopen.read()
        webopen.close()

        #3 - CONVERT page.html to Soup Object  
        #print(page_html)
        data = soup(page_html , 'html.parser')
        #datetime
        
        array = []
        for (i, item) in enumerate(data, start=1):
            any = item.text.split()
            # print(any)
            a1 = any[79] ; a2 = any[80] ; a3 = any[81] ; 
            b1 = any[83] ; b2 = any[84] ; b3 = any[85] ; 
            c1 = any[87] ; c2 = any[88] ; c3 = any[89] ; 
            d1 = any[91] ; d2 = any[92] ; d3 = any[93] ; 
            e1 = any[95] ; e2 = any[96] ; e3 = any[97] ; 
            f1 = any[99] ; f2 = any[100] ; f3 = any[101] ; 
            g1 = any[103] ; g2 = any[104] ; g3 = any[105] ; 
            h1 = any[107] ; h2 = any[108] ; h3 = any[109] ; 
            print(a1,b1,c1,d1,e1,f1,g1,h1)
            # print(a2,b2,c2,d2,e2,f2,g2,h2)
            # print(a3,b3,c3,d3,e3,f3,g3,h3)
            a = int(a1)
            b = int(b1)
            c = int(c1)
            d = int(d1)
            e = int(e1)
            f = int(f1)
            g = int(g1)
            h = int(h1)
            
        def time():
            today = datetime.datetime.today()
            hour = today.hour 
            min = today.minute  
            print(hour,min)
            if 1<= hour <= 3 and min == 00 :
                if a <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(a, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif a <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(a, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif a <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(a, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif a <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(a, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(a, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour <= 6 and min == 00:
                if b <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(b, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif b <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(b, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif b <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(b, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif b <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(b, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(b, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour <= 9 and min == 00:
                if c <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(c, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif c <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(c, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif c <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(c, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif c <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(c, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(c, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour <= 12 and min == 00:
                if d <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(d, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif d <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(d, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif d <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(d, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif d <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(d, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(d, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour <= 15 and min == 00:
                if e <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(e, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif e <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(e, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif e <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(e, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif e <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(e, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(e, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour <= 18 and min == 00:
                if f <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(f, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif f <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(f, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif f <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(f, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif f <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(f, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(f, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour <= 21 and min == 00:
                if g <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(g, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif g <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(g, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif g <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(g, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif g <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(g, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(g, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour <= 23 and min == 00:
                if h <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif h <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif h <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif h <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            elif hour == 00 and min == 00:
                print(h)
                if h <= 5 :     
                    grade = "G1"
                    lvel_kp = "เล็กน้อย"
                    print(lvel_kp ,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif h <= 6:
                    grade = "G2"
                    lvel_kp = "ปานกลาง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif h <= 7:
                    grade = "G3"
                    lvel_kp = "รุนแรง"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                elif h <= 8:
                    grade = "G4"
                    lvel_kp = "รุนแรงมาก"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
                else :
                    grade = "G5"
                    lvel_kp = "รุนแรงที่สุด"
                    print(lvel_kp,grade)
                    sql = "insert into kp_index(kp, avg_kp, level_kp) value(%s,%s,%s)" 
                    val=(h, grade,lvel_kp)
                    mycursor.execute(sql,val)
                    conn.commit(); 
            
                
                
        time()
    except Error as e:
        print(e)

    finally:
        if conn is not None and conn.is_connected():
            conn.close()


if __name__ == '__main__':
    connect()     
        #     number = a+b+c+d+e+f+g+h
        #     sum = number/8
        #     sum1 = number%8
            
        # def my_function(avg):   
        #     # arr = str.split()
        #     number = a+b+c+d+e+f+g+h
        #     sum = number/8
        #     sum1 = number%8
        #     if sum1 < 4 :
        #         avg = int(sum)
        #         print(avg)
        #         return avg
        #     else :
        #         avg = int(sum + 1)
        #         print(avg)
        #         return avg
        # # print(my_function(avg))
        # i = my_function(avg)
        # if i <= 5 :     
        #     grade = "G1"
        #     lvel_kp = "เล็กน้อย"
        #     print(lvel_kp ,grade)
        # elif i <= 6:
        #     grade = "G2"
        #     lvel_kp = "ปานกลาง"
        #     print(lvel_kp,grade)
        # elif i <= 7:
        #     grade = "G3"
        #     lvel_kp = "รุนแรง"
        #     print(lvel_kp,grade)
        # elif i <= 8:
        #     grade = "G4"
        #     lvel_kp = "รุนแรงมาก"
        #     print(lvel_kp,grade)
        # else :
        #     grade = "G5"
        #     lvel_kp = "รุนแรงที่สุด"
        #     print(lvel_kp,grade)