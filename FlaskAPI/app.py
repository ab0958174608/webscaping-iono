from email import message
from this import d
from flask import Flask
from flask import Flask
from flask_restful import Api,Resource,abort,reqparse,fields,marshal_with
import mysql.connector
from mysql.connector import Error
from scipy.fft import dst
import os
from threading import Timer
#db
def connect():
    """ Connect to MySQL database """
    conn = None
    try:
        conn = mysql.connector.connect(host='localhost',
                                       port='3306',
                                       database='web-iono',
                                       user='root',
                                       password='')
        if conn.is_connected():
            print('Connected to MySQL database')
        mycursor = conn.cursor()
        mycursor.execute("SELECT * FROM ap join kp_index join srstorm join radio join solarflux join dst")
        #     
        myresult = mycursor.fetchall()
        
        for x in myresult:
            # print(x[0],x[1],x[2])
            a = x
            # print(x[3],x[4],x[5],x[6],x[7])
            # print(x)
   
        app = Flask(__name__)
        api = Api(app)
        
       
       
        resource_fieldap={
                #Ap 
                "id_ap": fields.Integer,
                "ap" : fields.String,
                "avg_ap" : fields.String,
                "level_ap" : fields.String,
                "time":fields.DateTime,
            }
        resource_fieldkp={
                #Kp 
                "kp_id": fields.Integer,
                "kp" : fields.String,
                "avg_kp" : fields.String,
                "level_kp" : fields.String,
                "time":fields.DateTime,
        }
        resource_fieldsrs={
                #srs
                "id_srs": fields.Integer,
                "srs" : fields.String,
                "avg_srs" : fields.String,
                "level_storm" : fields.String,
                "time":fields.DateTime,
        }
        resource_fieldradio={
                #Radio 
                "radio_id": fields.Integer,
                "r1_r2" : fields.String,
                "avg_radio" : fields.String,
                "level_radio" : fields.String,
                "time":fields.DateTime,
        }
         
        resource_field={
                #flux
                "id_flux": fields.Integer,
                "adjusted_flux" : fields.String,
                "avg_flux" : fields.String,
                "level_flux" : fields.String,
                "time":fields.DateTime,
            
        }
        
        resource_fielddst={
                #dst 
                "id_dst": fields.Integer,
                "dst" : fields.String,
                "avg_dst" : fields.String,
                "level_dst" : fields.String,
                "time":fields.DateTime,
        }
            
        
        my_ap   = {"ap"  : {"id_ap" : x[0],"ap":x[1], "avg_ap":x[2], "level_ap":x[3], "time":x[4]}}
        my_kp   = {"kp"  : {"kp_id" : x[5],"kp":x[6], "avg_kp":x[7], "level_kp":x[8], "time":x[9]}}
        my_srs = {"srs": {"id_srs" : x[10],"srs":x[11], "avg_srs":x[12], "level_storm":x[13],"time":x[14]}}
        my_radio = {"radio": {"radio_id" : x[15],"r1_r2":x[16], "avg_radio":x[17],"level_radio":x[18],"time":x[19]}}
        my_flux = {"flux": {"id_flux" : x[20],"adjusted_flux":x[21],"avg_flux":x[22],"level_flux":x[23], "time":x[24]}}
        my_dst  = {"dst" : {"id_dst" : x[25],"dst":x[26], "level_dst":x[30],"time":x[28]}}   
        # my_knn  = {"knn" : {"id_flux" : x[0],"adjusted":x[1], "time":x[2]}}
        # #Validate request
        def notFoundCity(name):
            if name not in name:
                abort(404,message = "ไม่พบข้อมูลที่ต้องการ")
             
        
        class WeatherAp(Resource):
            @marshal_with(resource_fieldap)
            def get(self,name):
                notFoundCity(name)   
                return my_ap[name]
        class WeatherKp(Resource):
            @marshal_with(resource_fieldkp)
            def get(self,name):
                notFoundCity(name)   
                return my_kp[name]
        class WeatherSrs(Resource):
            @marshal_with(resource_fieldsrs)
            def get(self,name):
                notFoundCity(name)   
                return my_srs[name]
        class WeatherRadio(Resource):
            @marshal_with(resource_fieldradio)
            def get(self,name):
                notFoundCity(name)   
                return my_radio[name]
        class WeatherCity(Resource):
            @marshal_with(resource_field)
            def get(self,name):
                notFoundCity(name)   
                return my_flux[name]
        
        class WeatherDst(Resource):
            @marshal_with(resource_fielddst)
            def get(self,name):
                notFoundCity(name)   
                return my_dst[name]
            # @marshal_with(resource_field)
            # def post(self):
            #     return{"data":"Data Weather Post"}

        # #call
        api.add_resource(WeatherAp,"/weatherAp/<string:name>")
        api.add_resource(WeatherKp,"/weatherKp/<string:name>")
        api.add_resource(WeatherSrs,"/weatherSrs/<string:name>")
        api.add_resource(WeatherRadio,"/weatherRadio/<string:name>")
        api.add_resource(WeatherCity,"/weatherFlux/<string:name>")
        api.add_resource(WeatherDst,"/weatherDst/<string:name>")




        if __name__ == "__main__":
            app.run(debug=True)
    
    except Error as e:
        print(e)

    finally:
        if conn is not None and conn.is_connected():
            conn.close()


if __name__ == '__main__':
    connect()